package ru.t1.strelcov.tm.api.repository;

import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IBusinessRepository<Project> {
}
