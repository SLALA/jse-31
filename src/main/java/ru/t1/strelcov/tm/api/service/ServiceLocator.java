package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.IPropertyService;

public interface ServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

}
