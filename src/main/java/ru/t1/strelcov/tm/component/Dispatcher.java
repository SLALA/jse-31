package ru.t1.strelcov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.Operation;
import ru.t1.strelcov.tm.dto.request.AbstractRequest;
import ru.t1.strelcov.tm.dto.response.AbstractResponse;
import ru.t1.strelcov.tm.exception.system.UnknownServerRequestException;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, Operation<? extends AbstractRequest, ? extends AbstractResponse>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void register(@NotNull final Class<RQ> regClass, @NotNull final Operation<RQ, RS> operation) {
        map.put(regClass, operation);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @NotNull
    public Object call(@NotNull AbstractRequest request) {
        @Nullable final Operation operation = map.get(request.getClass());
        if (operation == null) throw new UnknownServerRequestException(request.getClass().getSimpleName());
        return operation.execute(request);
    }

}
