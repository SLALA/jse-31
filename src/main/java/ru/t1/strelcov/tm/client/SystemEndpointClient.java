package ru.t1.strelcov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;
import ru.t1.strelcov.tm.dto.response.ServerAboutResponse;
import ru.t1.strelcov.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @SneakyThrows
    @Override
    @NotNull
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        @NotNull final Object object = call(request);
        if (object instanceof Throwable) throw (Throwable) object;
        return (ServerAboutResponse) object;
    }

    @SneakyThrows
    @Override
    @NotNull
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        @NotNull final Object object = call(request);
        if (object instanceof Throwable) throw (Throwable) object;
        return (ServerVersionResponse) object;
    }

    public static void main(@Nullable final String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        System.out.println("[VERSION]:\n");
        @NotNull final ServerVersionResponse versionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(versionResponse.getVersion());
        System.out.println("[ABOUT]:\n");
        @NotNull final ServerAboutResponse aboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(aboutResponse.getName() + '\n' + aboutResponse.getEmail());
        client.disconnect();
    }

}
