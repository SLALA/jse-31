package ru.t1.strelcov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IPropertyService;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.service.PropertyService;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public abstract class AbstractEndpoint {

    @Nullable
    private Socket socket;

    @SneakyThrows
    protected void connect() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        socket = new Socket(propertyService.getServerHost(), propertyService.getServerPort());
    }

    @SneakyThrows
    protected void disconnect() {
        if (socket == null) return;
        socket.close();
    }

    @NotNull
    @SneakyThrows
    protected Object call(@NotNull final Object data) {
        if (socket == null) throw new AccessDeniedException();
        new ObjectOutputStream(socket.getOutputStream()).writeObject(data);
        return new ObjectInputStream(socket.getInputStream()).readObject();
    }

}
