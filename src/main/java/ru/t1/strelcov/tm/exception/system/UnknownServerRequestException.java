package ru.t1.strelcov.tm.exception.system;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class UnknownServerRequestException extends AbstractException {

    public UnknownServerRequestException(final String value) {
        super("Error: Unknown server request \"" + value + "\".");
    }

}
