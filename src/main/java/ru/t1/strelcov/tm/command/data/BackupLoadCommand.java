package ru.t1.strelcov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.Domain;
import ru.t1.strelcov.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "backup-load";
    }

    @Override
    @NotNull
    public String description() {
        return "Load entities data from backup file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull File file = new File(FILE_BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_BACKUP_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @Nullable final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
