package ru.t1.strelcov.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.component.Server;
import ru.t1.strelcov.tm.dto.request.AbstractRequest;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public final class ServerRequestTask extends AbstractServerSocketTask implements Runnable {

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server, socket);
    }

    @SneakyThrows
    @Override
    public void run() {
        @NotNull final InputStream inputStream = socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object inputObject = objectInputStream.readObject();
        @NotNull final AbstractRequest request = (AbstractRequest) inputObject;
        @NotNull final Object response = server.call(request);
        @Nullable final OutputStream outputStream = socket.getOutputStream();
        @Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(response);
        server.submit(new ServerRequestTask(server, socket));
    }

}
